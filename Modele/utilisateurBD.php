<?php 
/*Fonctions-modèle réalisant la gestion d'une table de la base,
** ou par extension gérant un ensemble de tables. 
** Les appels à ces fcts se fp,t dans c1.
*/

	//echo ("Penser à modifier les paramètres de connect.php avant l'inclusion du fichier <br/>");
	//require ("modele/connect.php") ; //connexion MYSQL et sélection de la base, $link défini
	

function verif_ident_BD($nom,$num,&$profil){ 
	require ("Modele/connectSQL.php");
	//global $pdo;
	$sql_etu="SELECT * FROM IDENT where login=:nom and mdp=:num";
	$res_etu= array(); 
	
	try {
		$cde_etu = $pdo->prepare($sql_etu);
		$cde_etu->bindParam(':nom', $nom);
		$cde_etu->bindParam(':num', $num);
		$b_etu = $cde_etu->execute();
		
		$res_etu = $cde_etu->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	
	if ((count($res_etu)> 0)) {
		$profil = $res_etu[0];	
		return true;
	}
	$profil = array();
	return false;
}

//Actualise la valeur bConnect du prof dans la BDD
function set_bconnect_profBD($id,$bconnect){
	require ("modele/connectSQL.php") ;
	$sql = "UPDATE professeur as p SET bConnect =:b WHERE p.id_prof=:id";
	
	$prep_sql = $pdo->prepare($sql);
	$prep_sql->bindParam(':b', $bconnect);
	$prep_sql->bindParam(':id', $id);
	$b_sql = $prep_sql->execute();
}

//Actualise la valeur bConnect de l'étudiant dans la BDD
function set_bconnect_etuBD($bconnect){
	require ("modele/connectSQL.php") ;
	$sql = "UPDATE etudiant as e SET bConnect =:b WHERE e.id_etu=:id";
	
	$prep_sql = $pdo->prepare($sql);
	$prep_sql->bindParam(':b', $bconnect);
	$prep_sql->bindParam(':id', $id);
	$b_sql = $prep_sql->execute();
}

?>
