<!doctype html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>identification</title>
  <link rel="stylesheet" href="vue/css/formStyle.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
</head>

<body>

	<div id="main">
		<h1> Formulaire d'authentification </h1> 
		<form action="index.php?controle=utilisateur&action=ident" method="post">

			<b>Nom</b> <br/><input name="nom" type="text" value= "<?php echo htmlspecialchars($nom);?>" placeholder="Entrez votre nom"/>
			<br/>
			<br/>
			<b>Mot de passe</b> <br/> <input  name="num"  type="password"  value= "<?php echo htmlspecialchars($num);?>" placeholder="Entrez votre mot de passe"/>		
			<br/><br/>
			<input class="btnSubmit" type= "submit" value="Soumettre"/>
			
		</form>

		<div id="error">
			<?php echo $msg;?>
		</div> 
	</div>
</body>
</html>
</html>
