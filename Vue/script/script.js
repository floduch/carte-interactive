var p=document.getElementById("demo");
var map;
var popup = L.popup();

//contient le code html de la popup qui apparit lors de la création d'un point
var popupHTML = "<form id='specMarker'>"+
                "<label for='titre'>Titre du point </label>"+
                "<input type='text' name='titre' id='titrePopup'required><br>"+
                "<label for='descPopup'>Descrition </label>"+
                "<input type='text' name='titre' id='descPopup'required><br>"+
                "<button id='btnSubmit' type='submit'>OK</button>"+
				"</form>";

//Permet de savoir si la popup d'un point créee par l'utilisateur est ouverte et non remplie
var isPopuping = false;

//Sert à savoir si l'initialisation de la map est en cours
var isInitialisation = true;

var datalayer;
window.onload = function(){
	map = L.map('map',{
		wheelPxPerZoomLevel: 50,	//vitesse de zoom en fonction de la vitesse du scroll
	}).setView([48.866667, 2.333333],18); 
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);
	getLocation();
	map.on('click', onMapClick);
};

/**
 * Retourne la position de l'utilisateur si le navigateur l'autorise
 */
function getLocation() {
	if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(showPosition);
	  }
		else{p.innerHTML="Geolocation non supportée par ce navigateur.";}
}

/**
 * 
 * @param {*} lat : latitude
 * @param {*} lon : longitude
 */
function setVision(lat, lon) {
	map.setView(new L.LatLng(lat, lon), 17);
	if(isInitialisation){
		setMap(lat,lon);
		isInitialisation = false;
	}
}
	  
/**
* showPosition(position) est invoqué par la fct getCurrentPosition(showPosition) de window.navigator.geolocation
* et position est instancié au moment de l'appel de la fonction, tel que 
* son attribut coords contient le positionnement du navigateur
**/
function showPosition(position) {
	setVision(position.coords.latitude, position.coords.longitude);
}

/**
 * Initialise les composents de la carte
 * @param {*} lat 
 * @param {*} lon 
 */
function setMap(lat, lon) {
	
	$.getJSON("./Vue/json/data.geojson",function(data){
		//creer le layer qui contient tout les points
		datalayer = L.geoJson(data ,{
			onEachFeature: function(feature, featureLayer) { //parcours le fichier et pour chaque feature execute le code suivant
				
				//Création de l'icône en fonction du nom
				var Icon = L.icon({
					iconUrl: './Vue/img/'+ feature.properties.reseau +'.jpg', /* Image à FOND TRANSPARENT !! */
					iconSize: [16, 16],
					iconAnchor: [8, 8],
					popupAnchor: [0, 0]
				});
				
				//Création des classes de caractère à filtrer - pas d'info dispo pour les rer CBDE ainsi que pour les trams
				let mask = /[CBDE]/g;
				let mask2=/[TL]/g;
				
				//numéro de station
				if (feature.properties.res_com.charAt(feature.properties.res_com.length-1).match(mask) || feature.properties.res_com.charAt(0).match(mask2))
					var numstation=feature.properties.res_com;
				
				else  {
				var numstation = feature.properties.res_com.substring(1,feature.properties.res_com.length);
				if(numstation.charAt(numstation.length-1)=="s")
					numstation=numstation.slice(0,-2);
				}
				
				// Horaire des métros
				  $.getJSON("https://api-ratp.pierre-grimaud.fr/v4/schedules/metros/"+numstation+"/"+feature.properties.nom_gare+"/R",function(data){
					  if(numstation.charAt(numstation.length-1).match(mask) || numstation.match(mask2)){
						  var horaire = "Information indisponible";
						  destination = "Information indisponible";}
					  else{
							var horaire = data.result.schedules[0].message;
							destination = data.result.schedules[0].destination;
						  }
				
					 $.getJSON("https://api-ratp.pierre-grimaud.fr/v4/schedules/metros/"+numstation+"/"+feature.properties.nom_gare+"/A",function(data2){
						 if(numstation.charAt(numstation.length-1).match(mask) || numstation.match(mask2)){
						  var horaire2 = "Information indisponible";
						  destination2 = "Information indisponible";}
						 else{
						  var horaire2 = data.result.schedules[0].message;
						  destination2 = data.result.schedules[0].destination;
						  }
					
				featureLayer.setIcon(Icon);
				featureLayer.bindPopup("<center>" + feature.properties.res_com + "<br>" + feature.properties.nom_gare + "</center>" //lie la popup avec le point
				+"<br>"+"Destination : "+destination2+": "+horaire2+"<br>"+"Destination : " +destination+": "+horaire);

				});
				});
			}
		}).addTo(map); //ajoute le layer contenant les points sur la carte
		map.fitBounds(datalayer.getBounds());
	});

	map.on('zoomend', function(e) { //au moment du zoom
    if (map.getZoom() < 13){ //si le niveau du zoom est inférieur à 13
            map.removeLayer(datalayer); //alors le layer est supprimé
    }
    else {
            map.addLayer(datalayer); //sinon on l'ajoute sur la map
        }
});
}

/**
 * Affiche le formulaire pour décrire le point qui vient d'être créée
 * Empêche le comportement de base
 * @param {*} e 
 */
function onMapClick(e) {
    if(!isPopuping){ //si une popup n'est pas déjà en cours de remplissage
        isPopuping = true;
        var marker = new L.marker(e.latlng, {icon: greenIcon}).addTo(map)
            //.bindPopup("<center>" + e.latlng.toString()+ "</center>"+'</br>'+photoImg).openPopup();
            .bindPopup(popupHTML,{
                keepInView: true,
                closeButton: false
                }).openPopup(); //ouverture de la popup

        //Permet de déplacer le marker
        marker.dragging.enable();
    }
    $("#specMarker").submit(function(e){ //ce déclenche lors de la soumission du formulaire
        e.preventDefault(); //annule le comportement de base
        marker.bindPopup($("#titrePopup").val()+"<br>" + $("#descPopup").val()).openPopup(); 
        isPopuping = false;
    });
}

//Création de l'icône personalisée
var greenIcon = L.icon({
    iconUrl: './Vue/img/leaf-green.png',
    shadowUrl: './Vue/img/leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var LeafIcon = L.Icon.extend({
    options: {
        shadowUrl: './Vue/img/leaf-shadow.png',
        iconSize:     [38, 95],
        shadowSize:   [50, 64],
        iconAnchor:   [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor:  [-3, -76]
    }
});

var greenIcon = new LeafIcon({iconUrl: './Vue/img/leaf-green.png'});	
    L.icon = function (options) {
    return new L.Icon(options);
};
