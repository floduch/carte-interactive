/*==================================================*/
/*TABLE : UTILISATEUR                               */
/*==================================================*/

CREATE TABLE IDENT
(
  LOGIN       VARCHAR(16),
  MDP         VARCHAR(32),
  NOM         VARCHAR(30),
  PRENOM      VARCHAR(30),
  PRIMARY KEY(LOGIN)
);

/*==================================================*/
/*TABLE : POINTCARTE                                */
/*==================================================*/

CREATE TABLE POINTCARTE
(
  IDPOINT     INT NOT NULL,
  IDUSER      VARCHAR(16),
  JSON        LONGTEXT,
  PRIMARY KEY(IDPOINT)
);

ALTER TABLE POINTCARTE
ADD FOREIGN KEY(IDUSER) REFERENCES IDENT(LOGIN);

CREATE SEQUENCE INCREPOINT
START WITH    1
INCREMENT BY  1
NOCACHE
NOCYCLE;
