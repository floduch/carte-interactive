<?php 
function ident() {
	$nom =  isset($_POST['nom'])?($_POST['nom']):'';
	$num =  isset($_POST['num'])?($_POST['num']):'';
	$mode = isset($_POST['mode'])?($_POST['mode']):'';
	$msg = '';
	
	// Au premier chargement
	if  (count($_POST)==0)
        require ("./vue/ident.tpl") ;
    
    else {

    	// utilisateur n'est pas dans la base on raffiche le formulaire vide
	    if (! verif_ident($nom,$num,$profil)) { 
			$_SESSION['profil'] = array();
			$msg ="erreur de saisie";
	        require ("./vue/ident.tpl");
		}
		// utilisateur est dans la base on appel
	    else {
			$_SESSION['profil'] = $profil; // on affiche son profil
			$url = "index.php?controle=utilisateur&action=carte";
			header ("Location:" .$url) ; // pour afficher le titre de l'onglet
			
			//$url = "accueil.php?no=$nom";
			//header ("Location:" .$url) ;	//echo ("ok, bienvenue"); 
		}
	}
}

function verif_ident($nom,$num,&$profil) {
	require ('./Modele/utilisateurBD.php');
	return verif_ident_BD($nom,$num,$profil); //true ou false en base;
}

function carte() {
	require('./controleur/carte.php');
	$url = "index.php?controle=carte&action=centrer"; // a voir pour changer
	header ("Location:" .$url);
}

//Vérifie si l'utlisateur est bien connecté sinon redirection
//vers la page de connexion
function is_login(){
	if($_SESSION == null Or $_SESSION['profil'] == null){
		$url = "index.php?controle=utilisateur&action=ident";
		header ("Location:" .$url);
	}
}

?>
